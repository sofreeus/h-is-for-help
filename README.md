﻿# H is for Help and documentation

Examples: ls, grep, kubectl, tar, rsync

---

## Overview

Help from the program binary
Help from the man or info system
Help from /usr/share/doc/
Help from the project website
Help from DuckDuckGo/Google

---

## program binary

What is happening when I ask for $PROGRAM? Am I getting a binary, an alias, a function, a bash internal, or what?

```bash
type $PROGRAM

# for bash internals
help $PROGRAM

# for most binaries, internals, and scripts, and some aliases and functions, too
$PROGRAM --help
```

Internal help function: Enter the program and then enter its help system. Example: vim

---

## man or info

Finding help from the command line.

---

### man

1. Usage ```man <software>``` e.g. ```man man```
2. Types of man pages:

```

       1      User Commands
       2      System Calls
       3      C Library Functions
       4      Devices and Special Files
       5      File Formats and Conventions
       6      Games et. Al.
       7      Miscellanea
       8      System Administration tools and Deamons

```

---

### ```man less```

By default, man uses *less* as its display program. 

To search in less, use vi commands:

* '/' e.g. `/quit`

Use up/down arrows to scroll through

'q' to quit *less*

---

### info

The `info` pages are an alternative to `man` pages. Most developers use one or the other. Usually, a call to `man $PROGRAM` or `info $PROGRAM` will yield a redirect or the documentation, but sometimes, the maintainers shipped docs in both systems. Check both when not finding enough information in one or the other.

---

## /usr/share/doc/

How to find the right docs folder?

```bash
# Which package owns this file?
rpm -qf $( which grep )
dpkg -S $( which grep )

# What other files did that package drop? Or, does that package have a `-docs` package?
dpkg -L grep
apt-get search snort
```

---

## project website

- [Samba](https://www.samba.org/samba/docs/)
- [Kubernetes](https://kubernetes.io/docs/home/)

---

## DuckDuckGo / Google

- +require
- -prohibit
- "make a phrase"
- site:slant.co ansible # search the domain slant.co for ansible using the search engine
- !(slant.co) ansible # search Slant for Ansible using Slant search # DDG only

---

## example exercises

- Which switch for rsync shows progress? Which abbreviates rplgotd (case?)?
- Which switch for dd shows progress?
- How to create a tar.bz2 archive? How to decompress one?
- Which switch for ls makes the size human readable? (also works for df)
- Which switch for df shows the filesystem type?
- How to grep a literal or a regex
- How to grep out some lines (vs grepping some lines)
- How to create an anon-share in Samba, NFS, FTP